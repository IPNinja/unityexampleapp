package com.yonatan.ipninjatest;

import android.support.multidex.MultiDexApplication;

import com.ipninja.sdk.IpNinjaApplication;

public class MyApp extends MultiDexApplication {

    public static final String IP_NINJA_SERVER = "138.128.241.16";
    public static final String AFF_ID = "aff-id";

    @Override
    public void onCreate() {
        super.onCreate();
        IpNinjaApplication.init(this, AFF_ID, IP_NINJA_SERVER);
    }

}